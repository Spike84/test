<?php

namespace App\Http\Controllers;

use App\Models\Prodhook;
use App\Models\Product;
use Illuminate\Http\Request;

class ProdhooksController extends Controller
{
	// метод получающий уведомление от веб-хука и добавляющий его в базу
    public function create()
    {
        $f = fopen('php://input', 'r');

		// Получаем содержимое потока
		$data = stream_get_contents($f);

		//сохраняем в базу уведомление полностью (в целом можно здесь вычленить ссылку и сохранять только ссылку на измененный товар)
		if (isset($data)) {
		     $input['content'] = $data;
		} else {
			$input['content'] = "";
		}

		//Пишем в базу тело уведомления
        $prodhook = Prodhook::create($input);
    }

	// метод обрабатывающий необработанные уведомления из базы (нужно в дальнейшем посадить на cron)
	public function process()
    {
		//пока выбираем по одной позиции, но в целом нужно сделать пачкой сразу по несколько штук
		$prodhook = Prodhook::select('id','content')
			->where([
				['is_processed', '=', 0],
			])
			->firstOrFail();

		if(empty($prodhook->content)) {
			//здесь что-то что случится если уведомление от веб-хука не записалось в базу
			die();
		}

		$data = json_decode($prodhook->content);

		$url = array();

		foreach($data->events as $event)
		{
			if ($event->action == "UPDATE") {
				$url[] = $event->meta->href;
			}
		};

		if(empty($url)) {
			//здесь что-то что случится если какаято ошибка с получением
			die();
		}

		//авторизация по токену (пока что здесь но по хорошему куда-то в настройки его вынести)
		$oauthToken = '3b44f64b1b95c1950752dfc7538d45930d85db80'; // токен, полученный в МойСклад

		//dump($url[0]);

		//весь процесс API запроса пока что здесь, но по-хорошему в отдельную функцию куда-то вынести
		$ch = curl_init($url[0]);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: $oauthToken"));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);

		$result = curl_exec($ch);

		curl_close($ch);
        //\\авторизация по токену

        $mass = json_decode($result);

		//dd($mass);

		//здесь нужно добавить какую-то дополнительную проверку на отсутствие ошибок в ответе API (например, что авторизация не пройдена или чтото еще)

		if (!isset($mass->salePrices)) {
			//здесь что-то что случится если какаято ошибка с получением цен
			die();
		}

		//ищем в базе товаров товар по указанному sku (в качестве sku я взял id товара)
		
		$product = Product::select('id')
			->where([
				['sku', '=', $mass->id],
			])
			->first();

		if (!isset($product)) {
			//здесь что-то, что случится если товар с таким sku не найден
			die();
		}
		

		//выбираем в данном случае цену по заданию, но в целом нужно все данные собрать и обновить их, т.к. из уведомления веб-хука не ясно что именно обновлялось
		//почему-то API выдает цену с двумя лишними нулями, поэтому делим на 100, но, в целом, нужно разобраться почему так
		$input['price'] = $mass->salePrices[0]->value/100;

		DB::beginTransaction();
		try {
			//обновляем товар
			$product->update($input);

			//обновляем статус 
			$prodhook->update(['is_processed' => 1]);
		} catch(\Illuminate\Database\QueryException $ex) {
			//здесь что-то в случае ошибки
		}
		DB::commit();
	}

}
